"""CombinedNumber modul"""


class CombinedNumber:
    """CombinedNumber class"""

    def __init__(self, input_list):
        self.list_of_num = []

    def add_element(self, number):
        if number >= 0:
            self.list_of_num.append(number)
        return self.list_of_num

    def convert_to_string(self):
        list_of_strings = []
        for num in self.list_of_num:
            list_of_strings.append(str(num))
        return list_of_strings

    def get_combined_number(self):
        result = ''

        if len(self.list_of_num) == 1:
            return self.convert_to_string()[0]

        list_str = self.convert_to_string()

        for i in range(len(list_str)):
            for j in range(i+1, len(list_str)):
                if (list_str[j] + list_str[i]) > (list_str[i] + list_str[j]):
                    list_str[i], list_str [j] = list_str[j], list_str [i]
        
        result = "".join(list_str)

        if result == '0'*len(list_str):
            result = '0'

        return result



