# Feature: Combined Number

As a customer 
I want to enter a list of non negative numbers
So that I could return their largest possible combined number as a string

## Scenario_1:

**Given**: Only one number is entered
**When**: The number is entered
**Then**: The result is converted number to a string

## Scenario_2:

**Given**: A list of numbers is entered
**When**: At least one of them is not 0
**Then**: The result is largest combined number as a string

## Scenario_3:

**Given**: A list of numbers is entered
**When**: All elements are 0
**Then**: The result is '0'