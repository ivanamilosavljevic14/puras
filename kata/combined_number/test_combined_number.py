"""Test modul for CombinedNumber class"""

import unittest

from combined_number import CombinedNumber


class TestCombinedNumber(unittest.TestCase):
    """Test class for CombinedNumber class"""

    def setUp(self):
        self.combined_number = CombinedNumber([])
    
    def test_should_confirm_that_add_method_work_correctly_with_positive_number(self):
        """Test for add_element method"""
        self.assertEqual([5], self.combined_number.add_element(5))
    
    def test_should_confirm_that_add_method_work_correctly_with_negative_number(self):
        """Test for add_element method"""
        self.assertEqual([], self.combined_number.add_element(-8))
    
    def test_should_confirm_that_convert_to_string_method_works_correctly(self):
        """Test for convert_to_string method"""
        self.combined_number.list_of_num = [7, 13]
        self.assertEqual(['7', '13'], self.combined_number.convert_to_string())   
    
    def test_should_confirm_that_output_is_correct_when_input_is_one_number(self):
        """Testing Scenario_1, entering only one number"""
        self.combined_number.list_of_num = [39]
        self.assertEqual('39', self.combined_number.get_combined_number())

    def test_should_confirm_that_output_is_correct_when_input_is_list_of_numbers(self):
        """Testing Scenario_2, entering a list of numbers"""
        self.combined_number.list_of_num = [50, 2, 1, 9]
        self.assertEqual('95021', self.combined_number.get_combined_number())

    def test_should_confirm_that_output_is_correct_when_input_is_list_of_zeros(self):
        """Testing Scenario_3, entering a list of zeros"""
        self.combined_number.list_of_num = [0, 0, 0]
        self.assertEqual('0', self.combined_number.get_combined_number())      
    

unittest.main()
